//
//  GameScene.m
//  p06-dhanakshirur
//
//  Created by Shashankjayate Dhanakshirur on 3/8/16.
//  Copyright (c) 2016 sdhanak1. All rights reserved.
//

#import "GameScene.h"
#import "GameOver.h"
#import <AVFoundation/AVFoundation.h>

@interface GameScene () <SKPhysicsContactDelegate>{
    SKSpriteNode* bird;
    SKColor *backgroundColor;
    SKTexture* _pipeTexture1;
    SKTexture* _pipeTexture2;
    SKAction* _moveAndRemovePipes;
    SKNode *movingNodes;
    BOOL canRestart;
    SKNode* pipes;
    NSInteger scoreInt;
    SKLabelNode *score;
    SKLabelNode *replay;
    SKSpriteNode *button;
    int counter;
}
@end

static const uint32_t birdCategory = 1 << 0;
static const uint32_t worldCategory = 1 << 1;
static const uint32_t pipeCategory = 1 << 2;
static const uint32_t scoreCategory = 1 << 3;

static NSInteger const kVerticalPipeGap = 280;

@implementation GameScene

-(void)didMoveToView:(SKView *)view {
    /* Setup your scene here */
    canRestart = NO;
    counter = 0;
    // Adding a spritekit node that includes everything that's moving except the bird
    movingNodes = [SKNode node];
    [self addChild:movingNodes];
    
    pipes = [SKNode node];
    [movingNodes addChild:pipes];

   

    SKTexture* birdTexture1 = [SKTexture textureWithImageNamed:@"stenger1"];
    birdTexture1.filteringMode = SKTextureFilteringNearest;
    SKTexture* birdTexture2 = [SKTexture textureWithImageNamed:@"stenger2"];
    birdTexture2.filteringMode = SKTextureFilteringNearest;
    
    SKAction* flap = [SKAction repeatActionForever:[SKAction animateWithTextures:@[birdTexture1, birdTexture2] timePerFrame:0.2]];
    
    bird = [SKSpriteNode spriteNodeWithTexture:birdTexture1];
    [bird setScale:0.20];
    NSLog(@"Width %f and height %f", self.frame.size.width, self.frame.size.height);
    bird.position = CGPointMake((self.frame.size.width/2.5), (self.frame.size.height - 100));
    [bird runAction:flap];
    
    //Adding physics to the bird
    bird.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:bird.size.height / 2];
    bird.physicsBody.dynamic = NO;
    bird.physicsBody.allowsRotation = NO;
    bird.physicsBody.categoryBitMask = birdCategory;
    bird.physicsBody.collisionBitMask = worldCategory | pipeCategory;
    bird.physicsBody.contactTestBitMask = worldCategory | pipeCategory;
    bird.zRotation = -M_PI/6.0f;
    [self addChild:bird];

    
    backgroundColor = [SKColor colorWithRed:113.0/255.0 green:197.0/255.0 blue:207.0/255.0 alpha:1.0];
    [self setBackgroundColor:backgroundColor];
    
    
    // Create ground
    
    SKTexture* groundTexture = [SKTexture textureWithImageNamed:@"Ground"];
    groundTexture.filteringMode = SKTextureFilteringNearest;
    NSLog(@"Loop till: %f", (2 + self.frame.size.width / ( groundTexture.size.width * 2 )));
    
    SKAction* moveGroundSprite = [SKAction moveByX:-groundTexture.size.width*2 y:0 duration:0.02 * groundTexture.size.width*2];
    SKAction* resetGroundSprite = [SKAction moveByX:groundTexture.size.width*2 y:0 duration:0];
    SKAction* moveGroundSpritesForever = [SKAction repeatActionForever:[SKAction sequence:@[moveGroundSprite, resetGroundSprite]]];

    
    for( int i = 0; i < 2 + self.frame.size.width / ( groundTexture.size.width * 2 ); i++) {
        SKSpriteNode* sprite = [SKSpriteNode spriteNodeWithTexture:groundTexture];
        [sprite setScale:2.0];
        sprite.position = CGPointMake(i * sprite.size.width, sprite.size.height/2);
        [sprite runAction:moveGroundSpritesForever];
        
        [movingNodes addChild:sprite];

        //[self addChild:sprite];
        
    }
    
    // Create ground physics container
    
    SKNode* dummy = [SKNode node];
    dummy.position = CGPointMake(0, groundTexture.size.height);
    dummy.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(self.frame.size.width, groundTexture.size.height * 2)];
    dummy.physicsBody.dynamic = NO;
    dummy.physicsBody.categoryBitMask = worldCategory;
    [self addChild:dummy];
    
   // sprite.position = CGPointMake(i * sprite.size.width, (sprite.size.height/3.5)*2);
 //   sprite.position = CGPointMake(i * sprite.size.width, sprite.size.height + groundTexture.size.height * 3);

    // Create skyline
    
    SKTexture* skylineTexture = [SKTexture textureWithImageNamed:@"Skyline"];
    skylineTexture.filteringMode = SKTextureFilteringNearest;
    SKAction* moveSkylineSprite = [SKAction moveByX:-skylineTexture.size.width*2 y:0 duration:0.1 * skylineTexture.size.width*2];
    SKAction* resetSkylineSprite = [SKAction moveByX:skylineTexture.size.width*2 y:0 duration:0];
    SKAction* moveSkylineSpritesForever = [SKAction repeatActionForever:[SKAction sequence:@[moveSkylineSprite, resetSkylineSprite]]];
    
    for( int i = 0; i < 2 + self.frame.size.width / ( skylineTexture.size.width * 2 ); i++ ) {
        SKSpriteNode* sprite = [SKSpriteNode spriteNodeWithTexture:skylineTexture];
        [sprite setScale:2.0];
        sprite.zPosition = -20;
        sprite.position = CGPointMake(i * sprite.size.width, sprite.size.height / 2 + groundTexture.size.height * 2);
        [sprite runAction:moveSkylineSpritesForever];
        //[self addChild:sprite];
        
        [movingNodes addChild:sprite];
    }
    
    //creating skyline_binghamton
    SKTexture* skylineTexture2 = [SKTexture textureWithImageNamed:@"skyline_2"];
    skylineTexture2.filteringMode = SKTextureFilteringNearest;
    SKAction* moveSkylineSprite2 = [SKAction moveByX:-skylineTexture2.size.width*2 y:0 duration:0.04 * skylineTexture2.size.width*2];
    SKAction* resetSkylineSprite2 = [SKAction moveByX:skylineTexture2.size.width*2 y:0 duration:0];
    SKAction* moveSkylineSpritesForever2 = [SKAction repeatActionForever:[SKAction sequence:@[moveSkylineSprite2, resetSkylineSprite2]]];
    
    for( int i = 0; i < 2 + self.frame.size.width / ( skylineTexture2.size.width * 2 ); i++ ) {
        SKSpriteNode* sprite = [SKSpriteNode spriteNodeWithTexture:skylineTexture2];
        [sprite setScale:2.0];
        sprite.zPosition = -18;
        sprite.position = CGPointMake(i * sprite.size.width, sprite.size.height / 2 + groundTexture.size.height * 2);
        [sprite runAction:moveSkylineSpritesForever2];

        [movingNodes addChild:sprite];
    }
    
    
    
    
    score = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    score.fontSize = 20;
    score.position = CGPointMake( CGRectGetMidX( self.frame ) + 120, 3.8 * self.frame.size.height / 4 );
    score.zPosition = 100;
    score.text = [NSString stringWithFormat:@"Score: %d", (int)scoreInt];
    [self addChild:score];
    
    //adding replay text after collision
    replay = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    replay.fontSize = 35;
    replay.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    [replay setText:@"Tap to Replay"];
    [self addChild:replay];
    replay.hidden = YES;
    
    
    button = [SKSpriteNode spriteNodeWithImageNamed:@"play_button"];
    button.position = CGPointMake(self.frame.size.width / 2 , self.frame.size.height / 2);
    [button setScale:0.75];
    [self addChild:button];
    movingNodes.speed = 0;
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    if( movingNodes.speed > 0 ){
        
        bird.physicsBody.velocity = CGVectorMake(0,0);
        [bird.physicsBody applyImpulse:CGVectorMake(0, 57)];
        [self runAction:[SKAction playSoundFileNamed:@"jump.wav" waitForCompletion:NO]];
      //  [self performSelector:@selector(scoreCounter) withObject:self afterDelay:2.0];
    }
    else if( canRestart ) {
        
        [self resetScene];
    }

}


-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
     bird.zRotation = clamp( -0.1, 0.1, bird.physicsBody.velocity.dy * ( bird.physicsBody.velocity.dy < 0 ? 0.002 : 0.001 ) );

}

CGFloat clamp(CGFloat min, CGFloat max, CGFloat value) {
    if( value > max ) {
        return max;
    } else if( value < min ) {
        return min;
    } else {
        return value;
    }
}

-(void)spawnPipes {
    SKNode* pipePair = [SKNode node];
    pipePair.position = CGPointMake( self.frame.size.width + _pipeTexture1.size.width, 0 );
    pipePair.zPosition = -10;
    
    CGFloat y = arc4random() % (NSInteger)( self.frame.size.height / 3 );
    
    SKSpriteNode* pipe1 = [SKSpriteNode spriteNodeWithTexture:_pipeTexture1];
    [pipe1 setScale:2];
    pipe1.position = CGPointMake( 0, y );
    pipe1.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:pipe1.size];
    pipe1.physicsBody.dynamic = NO;
    pipe1.physicsBody.categoryBitMask = pipeCategory;
    pipe1.physicsBody.contactTestBitMask = birdCategory;
    [pipePair addChild:pipe1];
    
    SKSpriteNode* pipe2 = [SKSpriteNode spriteNodeWithTexture:_pipeTexture2];
    [pipe2 setScale:2];
    pipe2.position = CGPointMake( 0, y + pipe1.size.height + kVerticalPipeGap );
    pipe2.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:pipe2.size];
    pipe2.physicsBody.dynamic = NO;
    pipe2.physicsBody.categoryBitMask = pipeCategory;
    pipe2.physicsBody.contactTestBitMask = birdCategory;
    [pipePair addChild:pipe2];
    
    //new SKNODe after every pipe to increment the score by 10
    SKNode *contactScore = [SKNode node];
    contactScore.position = CGPointMake(pipe1.size.width + bird.size.width /2, CGRectGetMidY(self.frame));
    contactScore.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(pipe2.size.width, self.frame.size.height)];
    contactScore.physicsBody.dynamic = NO;
    contactScore.physicsBody.categoryBitMask = scoreCategory;
    contactScore.physicsBody.contactTestBitMask = birdCategory;
    [pipePair addChild:contactScore];
    
    [pipePair runAction:_moveAndRemovePipes];
    
   // [self addChild:pipePair];
    
    //[movingNodes addChild:pipePair];
    [pipes addChild:pipePair];

}

- (void) didBeginContact:(SKPhysicsContact *)contact {
    NSLog(@"You lose with a score = %d", (int)scoreInt);
    
    if(movingNodes.speed > 0) {
        
        if((contact.bodyA.categoryBitMask & worldCategory) == worldCategory || (contact.bodyB.categoryBitMask & worldCategory) == worldCategory || (contact.bodyA.categoryBitMask & pipeCategory) == pipeCategory || (contact.bodyB.categoryBitMask & pipeCategory) == pipeCategory){
            //[self scoreCounter];
            [self runAction:[SKAction playSoundFileNamed:@"punch.wav" waitForCompletion:NO]];
            bird.physicsBody.velocity = CGVectorMake(0, 0);
        // [bird.physicsBody applyImpulse:CGVectorMake(0, 40)];
            [bird setPaused:YES];
            movingNodes.speed = 0;
            canRestart = YES;
            replay.hidden = NO;
        }
        
        else if((contact.bodyA.categoryBitMask & scoreCategory) == scoreCategory || (contact.bodyB.categoryBitMask & scoreCategory) == scoreCategory){
            [self scoreCounter];
        }
        
    }
    
    
}

-(void)resetScene {
    bird.position = CGPointMake((self.frame.size.width/2.5), (self.frame.size.height - 100));
    bird.physicsBody.velocity = CGVectorMake( 0, 0 );
    [bird setPaused:NO];
    // Remove all existing pipes
    [pipes removeAllChildren];
    
    // Reset _canRestart
    canRestart = NO;
    replay.hidden = YES;
    // Restart animation
    movingNodes.speed = 1;
    scoreInt = 0;
    score.text = [NSString stringWithFormat:@"Score: %d", (int)scoreInt];
}

-(void) scoreCounter {
    scoreInt = scoreInt + 10;
    score.text = [NSString stringWithFormat:@"Score: %d", (int)scoreInt];
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch;
    
    for(touch in touches){
       CGPoint location = 	[touch locationInNode:self];
        if([button containsPoint:location] && counter == 0){
            button.hidden = YES;
            //Changing gravity
            self.physicsWorld.gravity = CGVectorMake(0.0, -9.5);
            self.physicsWorld.contactDelegate = self;
            movingNodes.speed = 1;
            bird.physicsBody.dynamic = YES;
            
            // Create pipes
            
            _pipeTexture1 = [SKTexture textureWithImageNamed:@"pipe1"];
            _pipeTexture1.filteringMode = SKTextureFilteringNearest;
            _pipeTexture2 = [SKTexture textureWithImageNamed:@"pipe2"];
            _pipeTexture2.filteringMode = SKTextureFilteringNearest;
            
            CGFloat distanceToMove = self.frame.size.width + 2 * _pipeTexture1.size.width;
            SKAction* movePipes = [SKAction moveByX:-distanceToMove y:0 duration:0.01 * distanceToMove];
            SKAction* removePipes = [SKAction removeFromParent];
            _moveAndRemovePipes = [SKAction sequence:@[movePipes, removePipes]];
            
            SKAction* spawn = [SKAction performSelector:@selector(spawnPipes) onTarget:self];
            SKAction* delay = [SKAction waitForDuration:2.5];
            SKAction* spawnThenDelay = [SKAction sequence:@[spawn, delay]];
            SKAction* spawnThenDelayForever = [SKAction repeatActionForever:spawnThenDelay];
            [self runAction:spawnThenDelayForever];
            counter++;
        }
    }
}

@end
