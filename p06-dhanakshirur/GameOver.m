//
//  GameOver.m
//  p06-dhanakshirur
//
//  Created by Shashankjayate Dhanakshirur on 3/15/16.
//  Copyright © 2016 sdhanak1. All rights reserved.
//

#import "GameOver.h"
#import "GameScene.h"

@implementation GameOver

-(void) didMoveToView:(SKView *)view {
    SKColor* backgroundColor = [SKColor blueColor];
    [self setBackgroundColor:backgroundColor];
    
    SKLabelNode* gameOverLabel;
    gameOverLabel = [[SKLabelNode alloc] initWithFontNamed:@"GAME OVER"];
    gameOverLabel.fontSize = 50;
    gameOverLabel.zPosition = 20;
    gameOverLabel.fontColor = [UIColor blackColor];
    [self addChild:gameOverLabel];
    

    
}
- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    GameScene *gameScene = [[GameScene alloc] initWithSize: self.size];
    [self.view presentScene:gameScene];
    //NSLog(@"game %f",self.size);
}
@end
